import pandas as pd
import os
import glob
import shutil
import time

import os.path
import logging

os.chdir(r"C:\Projects\python\python_dir_files")

logging.basicConfig(filename='ouput_index.log', encoding='utf-8', level=logging.INFO)

start = time.time()

df2 = pd.read_excel('patient_list.xlsx')
df2['file_name_start'] = df2["MRN"].astype(str) + df2["Name"] + df2["Date of birth"].str.replace(r'/', '')

os.chdir(r"data")
for index, row in df2.iterrows():
    dirName = row['file_name_start']
    print(index, dirName)
    os.mkdir(dirName.replace(' ', ''))   # remove embedded spaces when creating directory
    count = 0
    src = dirName + "*.pdf"
    dest = dirName.replace(' ', '') + "\\"
    for name in glob.glob(src):
        new_path = shutil.move(name, dest)
        code = os.path.splitext(os.path.basename(new_path))[0][-3:]
        print('about to rename ' + new_path + ' to ' + new_path.replace(' ', ''))
        os.rename(new_path, new_path.replace(' ', ''))  # rename file in target directory
        logging.info(name + "|" + new_path.replace(' ', '') + "|" + row["Name"] + "|" + row["Date of birth"].replace(r'/', '') + "|" + code) 

elapsed_time = time.time() - start
print(f'Execution time: {elapsed_time} seconds')