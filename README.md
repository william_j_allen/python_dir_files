# README #

File Manipulation Assignment:
You are given a set of 300,000 pdf files, and a list of patients. Each file is named with the MRN, patient name, dob and a code for the file category, IE, "Notes."
You need to create a folder for each patient name, and move each file into the correct folder.

Additional requirements:

Remove the following elements from the filenames: "/",",","%"

## Output an index file after you have completed this exercise, with Filename, filepath, patient name, mrn, dob, file category

Output was done through logging.  Here are the first 10 rows of a 1000 row log file:

INFO:root:339902Savana Watson12282000ADT.pdf|339902SavanaWatson12282000\339902SavanaWatson12282000ADT.pdf|Savana Watson|12282000|ADT
INFO:root:339902Savana Watson12282000BAR.pdf|339902SavanaWatson12282000\339902SavanaWatson12282000BAR.pdf|Savana Watson|12282000|BAR
INFO:root:339902Savana Watson12282000CRM.pdf|339902SavanaWatson12282000\339902SavanaWatson12282000CRM.pdf|Savana Watson|12282000|CRM
INFO:root:339902Savana Watson12282000MDM.pdf|339902SavanaWatson12282000\339902SavanaWatson12282000MDM.pdf|Savana Watson|12282000|MDM
INFO:root:339902Savana Watson12282000ORU.pdf|339902SavanaWatson12282000\339902SavanaWatson12282000ORU.pdf|Savana Watson|12282000|ORU
INFO:root:528844Edith Brooks10261990ADT.pdf|528844EdithBrooks10261990\528844EdithBrooks10261990ADT.pdf|Edith Brooks|10261990|ADT
INFO:root:528844Edith Brooks10261990BAR.pdf|528844EdithBrooks10261990\528844EdithBrooks10261990BAR.pdf|Edith Brooks|10261990|BAR
INFO:root:528844Edith Brooks10261990CRM.pdf|528844EdithBrooks10261990\528844EdithBrooks10261990CRM.pdf|Edith Brooks|10261990|CRM
INFO:root:528844Edith Brooks10261990MDM.pdf|528844EdithBrooks10261990\528844EdithBrooks10261990MDM.pdf|Edith Brooks|10261990|MDM
INFO:root:528844Edith Brooks10261990ORU.pdf|528844EdithBrooks10261990\528844EdithBrooks10261990ORU.pdf|Edith Brooks|10261990|ORU


## Describe how you would go about this and what technologies you would use.

I used two related technologies to develop this functionality.  After creating some test data in Excel (patient_list.xlsx), I wrote the code in python 
using Jupyter Notebooks as a development environment, to create empty pdf files named based on the convention specified in the requirements.  I imported
some python libraries (os, glob, shutil, time, os.path, logging).  In addition I imported the pandas library which was used to read the 
patient_list.xlsx Excel file.

After several testing iterations, I was satisfied that the code created the files required, created a diretory for each patient, moved the related files 
into the employees directory, and renamed the files to eliminate spaces in the file name.

After I was satisfied with the results, I separated the file creation step (necessary because otherwise I had no data) in the makeFiles.py file, and the create directory 
and move related files step into the crDirAndMoveFiles.py file.

Then I created this README.md file