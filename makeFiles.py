# makeFile.py

import pandas as pd
import os

codes = ["ADT", "BAR", "MDM", "CRM", "ORU"]

df = pd.read_excel('patient_list.xlsx')
# replace backslash with dash on date since Windows will not create file name with backslash 
df['file_name_start'] = df["MRN"].astype(str) + df["Name"] + df["Date of birth"].str.replace(r'/', '')
#df['file_name_start']= df['file_name_start'].str.replace(r' ', '')
os.chdir(r"data")
for index, row in df.iterrows():
    print(row['file_name_start'])
    # access data using column names
    for code in codes:
        fileName = row['file_name_start']+code+'.pdf'
        print(fileName)
        f = open(fileName, "x")
        f.close()